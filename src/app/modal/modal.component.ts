import { Component, Input } from '@angular/core';
import { Launch } from '../launches.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @Input() showModal: boolean = false;
  @Input() launch: Launch | undefined;

  closeModal(): void {
    this.showModal = false;
  }
}
