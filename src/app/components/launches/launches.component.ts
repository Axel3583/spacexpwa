import { Component } from '@angular/core';
import { Launch } from 'src/app/launches.model';
import { CacheService } from 'src/app/service/cache/cache.service';
import { SpaceXService } from 'src/app/service/space-x.service';

@Component({
  selector: 'app-launches',
  templateUrl: './launches.component.html',
  styleUrls: ['./launches.component.css'],
})
export class LaunchesComponent {
  launch: any;
  searchTerm: string = '';
  searchResults: any[] = [];
  selectedLaunch: Launch | undefined;
  isShowModal: boolean = false;

  launches: Launch[] = [];
  apiUrl = 'https://api.spacexdata.com/v4/launches';

  constructor(
    private spaceX: SpaceXService,
    private cacheService: CacheService
  ) {}

  ngOnInit() {
    this.spaceX.getLaunches().subscribe((res) => {
      this.launch = res;
      this.searchResults = res;
    });

    const request = new Request(this.apiUrl, { method: 'GET' });
    this.cacheService.getFromCache(request).then((response) => {
      if (response) {
        console.log('Récupération réussie depuis le cache', response);
      } else {
        fetch(request)
          .then((response) => {
            this.cacheService.addToCache(request, response);
            console.log('Récupération réussie depuis le réseau', response);
          })
          .catch((error) => {
            console.log('Récupération échouée depuis le réseau', error);
          });
      }
    });
  }

  search(term: string): void {
    this.searchTerm = term;
    this.searchResults = this.launch.filter((res: any) =>
      res.name.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  openModal(launch: Launch): void {
    this.selectedLaunch = launch;
    this.isShowModal = true;
  }

  closeModal(): void {
    this.isShowModal = false;
  }
}
