import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  private cacheName = 'my-cache';

  constructor() { }

  async addToCache(request: Request, response: Response) {
    const cache = await caches.open(this.cacheName);
    await cache.put(request, response.clone());
  }

  async getFromCache(request: Request) {
    const cache = await caches.open(this.cacheName);
    const response = await cache.match(request);
    return response || null;
  }

  async removeCache() {
    return caches.delete(this.cacheName);
  }
}
