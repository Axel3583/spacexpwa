export interface Launch {
    id: number;
    name: string;
    details: string;
    image: string;
  }  